module gitlab.com/isard/isardvdi

go 1.16

require (
	github.com/ettle/strcase v0.1.1
	github.com/go-ldap/ldap/v3 v3.4.1
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/influxdata/influxdb-client-go/v2 v2.5.0
	github.com/rs/zerolog v1.23.0
	github.com/shirou/gopsutil/v3 v3.21.8
	github.com/spf13/viper v1.8.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	golang.org/x/oauth2 v0.0.0-20210402161424-2e8d93401602
	gopkg.in/rethinkdb/rethinkdb-go.v6 v6.2.1
	libvirt.org/go/libvirt v1.7009.0
)
